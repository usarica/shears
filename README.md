# Skimming

This repository provides tools to skim [NanoAOD](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookNanoAOD) datasets. It has evolved from the [Shears framework](https://gitlab.cern.ch/shears/shears). Software to process the skims is maintained in a [dedicated repository](https://gitlab.cern.ch/HZZ-IIHE/hzz2l2nu).


## Repository organisation

`Pruner` Program to perform event filtering in NanoAOD files producing smaller ROOT files (‘skims’).

`ntuple_production` Tools run the skimming over Grid.


## Installation recipe

Set up CMSSW:

```sh
cmsrel CMSSW_10_2_11
cd CMSSW_10_2_11/src 
cmsenv 
```

Clone and build this repository:

```sh
git clone ssh://git@gitlab.cern.ch:7999/HZZ-IIHE/shears.git
scramv1 b -j 4
```

Set up environment:

```sh
export PATH=$CMSSW_BASE/src/shears/ntuple_production:$PATH
```

## Examples

Producing a skim locally:

```sh
pruner --selection ZZ2l2vPruner --subselection MC_DLep \
  -o myskim.root --max-events 10000 --is-data=0 input_nanoaod.root
```
where `input_nanoaod.root` is an input file in the NanoAOD format.

Submitting jobs to Grid:

```sh
voms-proxy-init --voms cms
crab_submit config.yaml
```

where the format of the configuration file `config.yaml` is described in directory `ntuple_production`.

More details are provided in README files in the subdirectories.
