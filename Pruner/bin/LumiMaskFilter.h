#ifndef LUMIMASKFILTER_H_
#define LUMIMASKFILTER_H_

#include <map>
#include <string>
#include <utility>
#include <vector>


/**
 * \brief Implements filtering accoring to a luminosity mask
 *
 * The luminosity mask must be given as a JSON file in the standard PPD format.
 */
class LumiMaskFilter {
 public:
  /// Constructor from a path to JSON file with luminosity mask
  LumiMaskFilter(std::string const &path);

  /// Checks if given run and luminosity section are included in the mask
  bool Pass(int64_t run, int64_t ls) const;

 private:
  using LumiRange = std::pair<int64_t, int64_t>;

  /// Reads luminosity mask from given JSON file
  void LoadLumiMask(std::string const &path);

  /// Implementation for the check against the luminosity mask
  bool PassImpl(int64_t run, int64_t ls) const;

  /**
   * \brief Luminosity mask
   *
   * Vector of LS ranges is sorted.
   */
  std::map<int64_t, std::vector<LumiRange>> lumiMask;

  /// Cached run and LS numbers from most recent call to Pass
  mutable int64_t cachedRun, cachedLS;

  /// Cached result from most recent call to Pass
  mutable bool cachedResult;
};

#endif  // LUMIMASKFILTER_H_

