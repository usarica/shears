#include "TriggerFilter.h"

#include <iostream>


TriggerFilter::TriggerFilter(std::vector<std::string> const &triggers) {
  SetTriggers(triggers);
}


void TriggerFilter::Init(TTreeReader &reader) {
  TTree *tree = reader.GetTree();

  for (auto const &trigger : triggerNames_) {
    // Skip triggers that do not correspond to any brances
    if (not tree->GetBranch(trigger.c_str()))
      continue;

    triggerBits_.emplace_back(
      new TTreeReaderValue<Bool_t>(reader, trigger.c_str()));
  }

  if (triggerBits_.empty()) {
    std::cerr << "None of triggers were found in the source tree. This filter "
        "will reject all events. The following triggers were requested:\n";

    for (auto const &trigger : triggerNames_)
      std::cerr << "  \"" << trigger << "\"\n";
  }
}


bool TriggerFilter::operator()() const {
  // Evaluate OR of all triggers
  for (auto &bit : triggerBits_) {
    if (*bit->Get())
      return true;
  }

  // If this point is reached, none of the triggers accepted the event
  return false;
}


void TriggerFilter::SetTriggers(std::vector<std::string> const &triggers) {
  triggerNames_ = triggers;
}

