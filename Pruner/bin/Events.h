//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu May  9 16:28:01 2019 by ROOT version 5.34/38
// from TTree Events/Events
// found on file: ZZTo2L2Nu_13TeV_powheg_pythia8_RunIISummer16_NanoAODv4.root
//////////////////////////////////////////////////////////

#ifndef Events_h
#define Events_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class Events {
public :
   TTreeReader     fReader;  //!the tree reader
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Readers to access the data (delete the ones you do not need).
   TTreeReaderValue<UInt_t> runNumber = {fReader, "run"};
   TTreeReaderValue<UInt_t> luminosityBlock = {fReader, "luminosityBlock"};
   TTreeReaderValue<ULong64_t> event = {fReader, "event"};
   TTreeReaderValue<Float_t> CaloMET_phi = {fReader, "CaloMET_phi"};
   TTreeReaderValue<Float_t> CaloMET_pt = {fReader, "CaloMET_pt"};
   TTreeReaderValue<Float_t> CaloMET_sumEt = {fReader, "CaloMET_sumEt"};
   TTreeReaderValue<Float_t> ChsMET_phi = {fReader, "ChsMET_phi"};
   TTreeReaderValue<Float_t> ChsMET_pt = {fReader, "ChsMET_pt"};
   TTreeReaderValue<Float_t> ChsMET_sumEt = {fReader, "ChsMET_sumEt"};
   TTreeReaderValue<UInt_t> nElectron = {fReader, "nElectron"};
   TTreeReaderArray<Float_t> Electron_deltaEtaSC = {fReader, "Electron_deltaEtaSC"};
   TTreeReaderArray<Float_t> Electron_dr03EcalRecHitSumEt = {fReader, "Electron_dr03EcalRecHitSumEt"};
   TTreeReaderArray<Float_t> Electron_dr03HcalDepth1TowerSumEt = {fReader, "Electron_dr03HcalDepth1TowerSumEt"};
   TTreeReaderArray<Float_t> Electron_dr03TkSumPt = {fReader, "Electron_dr03TkSumPt"};
   TTreeReaderArray<Float_t> Electron_dr03TkSumPtHEEP = {fReader, "Electron_dr03TkSumPtHEEP"};
   TTreeReaderArray<Float_t> Electron_dxy = {fReader, "Electron_dxy"};
   TTreeReaderArray<Float_t> Electron_dxyErr = {fReader, "Electron_dxyErr"};
   TTreeReaderArray<Float_t> Electron_dz = {fReader, "Electron_dz"};
   TTreeReaderArray<Float_t> Electron_dzErr = {fReader, "Electron_dzErr"};
   TTreeReaderArray<Float_t> Electron_eCorr = {fReader, "Electron_eCorr"};
   TTreeReaderArray<Float_t> Electron_eInvMinusPInv = {fReader, "Electron_eInvMinusPInv"};
   TTreeReaderArray<Float_t> Electron_energyErr = {fReader, "Electron_energyErr"};
   TTreeReaderArray<Float_t> Electron_eta = {fReader, "Electron_eta"};
   TTreeReaderArray<Float_t> Electron_hoe = {fReader, "Electron_hoe"};
   TTreeReaderArray<Float_t> Electron_ip3d = {fReader, "Electron_ip3d"};
   TTreeReaderArray<Float_t> Electron_jetRelIso = {fReader, "Electron_jetRelIso"};
   TTreeReaderArray<Float_t> Electron_mass = {fReader, "Electron_mass"};
   TTreeReaderArray<Float_t> Electron_miniPFRelIso_all = {fReader, "Electron_miniPFRelIso_all"};
   TTreeReaderArray<Float_t> Electron_miniPFRelIso_chg = {fReader, "Electron_miniPFRelIso_chg"};
   TTreeReaderArray<Float_t> Electron_mvaFall17V1Iso = {fReader, "Electron_mvaFall17V1Iso"};
   TTreeReaderArray<Float_t> Electron_mvaFall17V1noIso = {fReader, "Electron_mvaFall17V1noIso"};
   TTreeReaderArray<Float_t> Electron_mvaFall17V2Iso = {fReader, "Electron_mvaFall17V2Iso"};
   TTreeReaderArray<Float_t> Electron_mvaFall17V2noIso = {fReader, "Electron_mvaFall17V2noIso"};
   TTreeReaderArray<Float_t> Electron_pfRelIso03_all = {fReader, "Electron_pfRelIso03_all"};
   TTreeReaderArray<Float_t> Electron_pfRelIso03_chg = {fReader, "Electron_pfRelIso03_chg"};
   TTreeReaderArray<Float_t> Electron_phi = {fReader, "Electron_phi"};
   TTreeReaderArray<Float_t> Electron_pt = {fReader, "Electron_pt"};
   TTreeReaderArray<Float_t> Electron_r9 = {fReader, "Electron_r9"};
   TTreeReaderArray<Float_t> Electron_sieie = {fReader, "Electron_sieie"};
   TTreeReaderArray<Float_t> Electron_sip3d = {fReader, "Electron_sip3d"};
   TTreeReaderArray<Float_t> Electron_mvaTTH = {fReader, "Electron_mvaTTH"};
   TTreeReaderArray<Int_t> Electron_charge = {fReader, "Electron_charge"};
   TTreeReaderArray<Int_t> Electron_cutBased = {fReader, "Electron_cutBased"};
   TTreeReaderArray<Int_t> Electron_cutBased_Fall17_V1 = {fReader, "Electron_cutBased_Fall17_V1"};
   TTreeReaderArray<Int_t> Electron_jetIdx = {fReader, "Electron_jetIdx"};
   TTreeReaderArray<Int_t> Electron_pdgId = {fReader, "Electron_pdgId"};
   TTreeReaderArray<Int_t> Electron_photonIdx = {fReader, "Electron_photonIdx"};
   TTreeReaderArray<Int_t> Electron_tightCharge = {fReader, "Electron_tightCharge"};
   TTreeReaderArray<Int_t> Electron_vidNestedWPBitmap = {fReader, "Electron_vidNestedWPBitmap"};
   TTreeReaderArray<Bool_t> Electron_convVeto = {fReader, "Electron_convVeto"};
   TTreeReaderArray<Bool_t> Electron_cutBased_HEEP = {fReader, "Electron_cutBased_HEEP"};
   TTreeReaderArray<Bool_t> Electron_isPFcand = {fReader, "Electron_isPFcand"};
   TTreeReaderArray<UChar_t> Electron_lostHits = {fReader, "Electron_lostHits"};
   TTreeReaderArray<Bool_t> Electron_mvaFall17V1Iso_WP80 = {fReader, "Electron_mvaFall17V1Iso_WP80"};
   TTreeReaderArray<Bool_t> Electron_mvaFall17V1Iso_WP90 = {fReader, "Electron_mvaFall17V1Iso_WP90"};
   TTreeReaderArray<Bool_t> Electron_mvaFall17V1Iso_WPL = {fReader, "Electron_mvaFall17V1Iso_WPL"};
   TTreeReaderArray<Bool_t> Electron_mvaFall17V1noIso_WP80 = {fReader, "Electron_mvaFall17V1noIso_WP80"};
   TTreeReaderArray<Bool_t> Electron_mvaFall17V1noIso_WP90 = {fReader, "Electron_mvaFall17V1noIso_WP90"};
   TTreeReaderArray<Bool_t> Electron_mvaFall17V1noIso_WPL = {fReader, "Electron_mvaFall17V1noIso_WPL"};
   TTreeReaderArray<Bool_t> Electron_mvaFall17V2Iso_WP80 = {fReader, "Electron_mvaFall17V2Iso_WP80"};
   TTreeReaderArray<Bool_t> Electron_mvaFall17V2Iso_WP90 = {fReader, "Electron_mvaFall17V2Iso_WP90"};
   TTreeReaderArray<Bool_t> Electron_mvaFall17V2Iso_WPL = {fReader, "Electron_mvaFall17V2Iso_WPL"};
   TTreeReaderArray<Bool_t> Electron_mvaFall17V2noIso_WP80 = {fReader, "Electron_mvaFall17V2noIso_WP80"};
   TTreeReaderArray<Bool_t> Electron_mvaFall17V2noIso_WP90 = {fReader, "Electron_mvaFall17V2noIso_WP90"};
   TTreeReaderArray<Bool_t> Electron_mvaFall17V2noIso_WPL = {fReader, "Electron_mvaFall17V2noIso_WPL"};
   TTreeReaderValue<UInt_t> nJet = {fReader, "nJet"};
   TTreeReaderArray<Float_t> Jet_area = {fReader, "Jet_area"};
   TTreeReaderArray<Float_t> Jet_btagCMVA = {fReader, "Jet_btagCMVA"};
   TTreeReaderArray<Float_t> Jet_btagCSVV2 = {fReader, "Jet_btagCSVV2"};
   TTreeReaderArray<Float_t> Jet_btagDeepB = {fReader, "Jet_btagDeepB"};
   TTreeReaderArray<Float_t> Jet_btagDeepC = {fReader, "Jet_btagDeepC"};
   TTreeReaderArray<Float_t> Jet_btagDeepFlavB = {fReader, "Jet_btagDeepFlavB"};
   TTreeReaderArray<Float_t> Jet_chEmEF = {fReader, "Jet_chEmEF"};
   TTreeReaderArray<Float_t> Jet_chHEF = {fReader, "Jet_chHEF"};
   TTreeReaderArray<Float_t> Jet_eta = {fReader, "Jet_eta"};
   TTreeReaderArray<Float_t> Jet_mass = {fReader, "Jet_mass"};
   TTreeReaderArray<Float_t> Jet_muEF = {fReader, "Jet_muEF"};
   TTreeReaderArray<Float_t> Jet_neEmEF = {fReader, "Jet_neEmEF"};
   TTreeReaderArray<Float_t> Jet_neHEF = {fReader, "Jet_neHEF"};
   TTreeReaderArray<Float_t> Jet_phi = {fReader, "Jet_phi"};
   TTreeReaderArray<Float_t> Jet_pt = {fReader, "Jet_pt"};
   TTreeReaderArray<Float_t> Jet_qgl = {fReader, "Jet_qgl"};
   TTreeReaderArray<Float_t> Jet_rawFactor = {fReader, "Jet_rawFactor"};
   TTreeReaderArray<Float_t> Jet_bRegCorr = {fReader, "Jet_bRegCorr"};
   TTreeReaderArray<Float_t> Jet_bRegRes = {fReader, "Jet_bRegRes"};
   TTreeReaderArray<Int_t> Jet_electronIdx1 = {fReader, "Jet_electronIdx1"};
   TTreeReaderArray<Int_t> Jet_electronIdx2 = {fReader, "Jet_electronIdx2"};
   TTreeReaderArray<Int_t> Jet_jetId = {fReader, "Jet_jetId"};
   TTreeReaderArray<Int_t> Jet_muonIdx1 = {fReader, "Jet_muonIdx1"};
   TTreeReaderArray<Int_t> Jet_muonIdx2 = {fReader, "Jet_muonIdx2"};
   TTreeReaderArray<Int_t> Jet_nConstituents = {fReader, "Jet_nConstituents"};
   TTreeReaderArray<Int_t> Jet_nElectrons = {fReader, "Jet_nElectrons"};
   TTreeReaderArray<Int_t> Jet_nMuons = {fReader, "Jet_nMuons"};
   TTreeReaderArray<Int_t> Jet_puId = {fReader, "Jet_puId"};
   TTreeReaderValue<Float_t> MET_MetUnclustEnUpDeltaX = {fReader, "MET_MetUnclustEnUpDeltaX"};
   TTreeReaderValue<Float_t> MET_MetUnclustEnUpDeltaY = {fReader, "MET_MetUnclustEnUpDeltaY"};
   TTreeReaderValue<Float_t> MET_phi = {fReader, "MET_phi"};
   TTreeReaderValue<Float_t> MET_pt = {fReader, "MET_pt"};
   TTreeReaderValue<Float_t> MET_sumEt = {fReader, "MET_sumEt"};
   TTreeReaderValue<UInt_t> nMuon = {fReader, "nMuon"};
   TTreeReaderArray<Float_t> Muon_dxy = {fReader, "Muon_dxy"};
   TTreeReaderArray<Float_t> Muon_dxyErr = {fReader, "Muon_dxyErr"};
   TTreeReaderArray<Float_t> Muon_dz = {fReader, "Muon_dz"};
   TTreeReaderArray<Float_t> Muon_dzErr = {fReader, "Muon_dzErr"};
   TTreeReaderArray<Float_t> Muon_eta = {fReader, "Muon_eta"};
   TTreeReaderArray<Float_t> Muon_ip3d = {fReader, "Muon_ip3d"};
   TTreeReaderArray<Float_t> Muon_jetRelIso = {fReader, "Muon_jetRelIso"};
   TTreeReaderArray<Float_t> Muon_mass = {fReader, "Muon_mass"};
   TTreeReaderArray<Float_t> Muon_miniPFRelIso_all = {fReader, "Muon_miniPFRelIso_all"};
   TTreeReaderArray<Float_t> Muon_miniPFRelIso_chg = {fReader, "Muon_miniPFRelIso_chg"};
   TTreeReaderArray<Float_t> Muon_pfRelIso03_all = {fReader, "Muon_pfRelIso03_all"};
   TTreeReaderArray<Float_t> Muon_pfRelIso03_chg = {fReader, "Muon_pfRelIso03_chg"};
   TTreeReaderArray<Float_t> Muon_pfRelIso04_all = {fReader, "Muon_pfRelIso04_all"};
   TTreeReaderArray<Float_t> Muon_phi = {fReader, "Muon_phi"};
   TTreeReaderArray<Float_t> Muon_pt = {fReader, "Muon_pt"};
   TTreeReaderArray<Float_t> Muon_ptErr = {fReader, "Muon_ptErr"};
   TTreeReaderArray<Float_t> Muon_segmentComp = {fReader, "Muon_segmentComp"};
   TTreeReaderArray<Float_t> Muon_sip3d = {fReader, "Muon_sip3d"};
   TTreeReaderArray<Float_t> Muon_mvaTTH = {fReader, "Muon_mvaTTH"};
   TTreeReaderArray<Int_t> Muon_charge = {fReader, "Muon_charge"};
   TTreeReaderArray<Int_t> Muon_jetIdx = {fReader, "Muon_jetIdx"};
   TTreeReaderArray<Int_t> Muon_nStations = {fReader, "Muon_nStations"};
   TTreeReaderArray<Int_t> Muon_nTrackerLayers = {fReader, "Muon_nTrackerLayers"};
   TTreeReaderArray<Int_t> Muon_pdgId = {fReader, "Muon_pdgId"};
   TTreeReaderArray<Int_t> Muon_tightCharge = {fReader, "Muon_tightCharge"};
   TTreeReaderArray<UChar_t> Muon_highPtId = {fReader, "Muon_highPtId"};
   TTreeReaderArray<Bool_t> Muon_inTimeMuon = {fReader, "Muon_inTimeMuon"};
   TTreeReaderArray<Bool_t> Muon_isGlobal = {fReader, "Muon_isGlobal"};
   TTreeReaderArray<Bool_t> Muon_isPFcand = {fReader, "Muon_isPFcand"};
   TTreeReaderArray<Bool_t> Muon_isTracker = {fReader, "Muon_isTracker"};
   TTreeReaderArray<Bool_t> Muon_mediumId = {fReader, "Muon_mediumId"};
   TTreeReaderArray<Bool_t> Muon_mediumPromptId = {fReader, "Muon_mediumPromptId"};
   TTreeReaderArray<UChar_t> Muon_miniIsoId = {fReader, "Muon_miniIsoId"};
   TTreeReaderArray<UChar_t> Muon_multiIsoId = {fReader, "Muon_multiIsoId"};
   TTreeReaderArray<UChar_t> Muon_mvaId = {fReader, "Muon_mvaId"};
   TTreeReaderArray<UChar_t> Muon_pfIsoId = {fReader, "Muon_pfIsoId"};
   TTreeReaderArray<Bool_t> Muon_softId = {fReader, "Muon_softId"};
   TTreeReaderArray<Bool_t> Muon_softMvaId = {fReader, "Muon_softMvaId"};
   TTreeReaderArray<Bool_t> Muon_tightId = {fReader, "Muon_tightId"};
   TTreeReaderArray<UChar_t> Muon_tkIsoId = {fReader, "Muon_tkIsoId"};
   TTreeReaderArray<Bool_t> Muon_triggerIdLoose = {fReader, "Muon_triggerIdLoose"};
   TTreeReaderValue<UInt_t> nPhoton = {fReader, "nPhoton"};
   TTreeReaderArray<Float_t> Photon_eCorr = {fReader, "Photon_eCorr"};
   TTreeReaderArray<Float_t> Photon_energyErr = {fReader, "Photon_energyErr"};
   TTreeReaderArray<Float_t> Photon_eta = {fReader, "Photon_eta"};
   TTreeReaderArray<Float_t> Photon_hoe = {fReader, "Photon_hoe"};
   TTreeReaderArray<Float_t> Photon_mass = {fReader, "Photon_mass"};
   TTreeReaderArray<Float_t> Photon_mvaID = {fReader, "Photon_mvaID"};
   TTreeReaderArray<Float_t> Photon_pfRelIso03_all = {fReader, "Photon_pfRelIso03_all"};
   TTreeReaderArray<Float_t> Photon_pfRelIso03_chg = {fReader, "Photon_pfRelIso03_chg"};
   TTreeReaderArray<Float_t> Photon_phi = {fReader, "Photon_phi"};
   TTreeReaderArray<Float_t> Photon_pt = {fReader, "Photon_pt"};
   TTreeReaderArray<Float_t> Photon_r9 = {fReader, "Photon_r9"};
   TTreeReaderArray<Float_t> Photon_sieie = {fReader, "Photon_sieie"};
   TTreeReaderArray<Int_t> Photon_charge = {fReader, "Photon_charge"};
   TTreeReaderArray<Int_t> Photon_electronIdx = {fReader, "Photon_electronIdx"};
   TTreeReaderArray<Int_t> Photon_jetIdx = {fReader, "Photon_jetIdx"};
   TTreeReaderArray<Int_t> Photon_pdgId = {fReader, "Photon_pdgId"};
   TTreeReaderArray<Int_t> Photon_vidNestedWPBitmap = {fReader, "Photon_vidNestedWPBitmap"};
   TTreeReaderArray<Bool_t> Photon_electronVeto = {fReader, "Photon_electronVeto"};
   TTreeReaderArray<Bool_t> Photon_isScEtaEB = {fReader, "Photon_isScEtaEB"};
   TTreeReaderArray<Bool_t> Photon_isScEtaEE = {fReader, "Photon_isScEtaEE"};
   TTreeReaderArray<Bool_t> Photon_mvaID_WP80 = {fReader, "Photon_mvaID_WP80"};
   TTreeReaderArray<Bool_t> Photon_mvaID_WP90 = {fReader, "Photon_mvaID_WP90"};
   TTreeReaderArray<Bool_t> Photon_pixelSeed = {fReader, "Photon_pixelSeed"};
   TTreeReaderValue<Float_t> PuppiMET_phi = {fReader, "PuppiMET_phi"};
   TTreeReaderValue<Float_t> PuppiMET_pt = {fReader, "PuppiMET_pt"};
   TTreeReaderValue<Float_t> PuppiMET_sumEt = {fReader, "PuppiMET_sumEt"};
   TTreeReaderValue<Float_t> RawMET_phi = {fReader, "RawMET_phi"};
   TTreeReaderValue<Float_t> RawMET_pt = {fReader, "RawMET_pt"};
   TTreeReaderValue<Float_t> RawMET_sumEt = {fReader, "RawMET_sumEt"};
   TTreeReaderValue<Float_t> fixedGridRhoFastjetAll = {fReader, "fixedGridRhoFastjetAll"};
   TTreeReaderValue<Float_t> fixedGridRhoFastjetCentralCalo = {fReader, "fixedGridRhoFastjetCentralCalo"};
   TTreeReaderValue<Float_t> fixedGridRhoFastjetCentralNeutral = {fReader, "fixedGridRhoFastjetCentralNeutral"};
   TTreeReaderValue<UInt_t> nSoftActivityJet = {fReader, "nSoftActivityJet"};
   TTreeReaderArray<Float_t> SoftActivityJet_eta = {fReader, "SoftActivityJet_eta"};
   TTreeReaderArray<Float_t> SoftActivityJet_phi = {fReader, "SoftActivityJet_phi"};
   TTreeReaderArray<Float_t> SoftActivityJet_pt = {fReader, "SoftActivityJet_pt"};
   TTreeReaderValue<Float_t> SoftActivityJetHT = {fReader, "SoftActivityJetHT"};
   TTreeReaderValue<Float_t> SoftActivityJetHT10 = {fReader, "SoftActivityJetHT10"};
   TTreeReaderValue<Float_t> SoftActivityJetHT2 = {fReader, "SoftActivityJetHT2"};
   TTreeReaderValue<Float_t> SoftActivityJetHT5 = {fReader, "SoftActivityJetHT5"};
   TTreeReaderValue<Int_t> SoftActivityJetNjets10 = {fReader, "SoftActivityJetNjets10"};
   TTreeReaderValue<Int_t> SoftActivityJetNjets2 = {fReader, "SoftActivityJetNjets2"};
   TTreeReaderValue<Int_t> SoftActivityJetNjets5 = {fReader, "SoftActivityJetNjets5"};
   TTreeReaderValue<UInt_t> nSubJet = {fReader, "nSubJet"};
   TTreeReaderArray<Float_t> SubJet_btagCMVA = {fReader, "SubJet_btagCMVA"};
   TTreeReaderArray<Float_t> SubJet_btagCSVV2 = {fReader, "SubJet_btagCSVV2"};
   TTreeReaderArray<Float_t> SubJet_btagDeepB = {fReader, "SubJet_btagDeepB"};
   TTreeReaderArray<Float_t> SubJet_eta = {fReader, "SubJet_eta"};
   TTreeReaderArray<Float_t> SubJet_mass = {fReader, "SubJet_mass"};
   TTreeReaderArray<Float_t> SubJet_n2b1 = {fReader, "SubJet_n2b1"};
   TTreeReaderArray<Float_t> SubJet_n3b1 = {fReader, "SubJet_n3b1"};
   TTreeReaderArray<Float_t> SubJet_phi = {fReader, "SubJet_phi"};
   TTreeReaderArray<Float_t> SubJet_pt = {fReader, "SubJet_pt"};
   TTreeReaderArray<Float_t> SubJet_rawFactor = {fReader, "SubJet_rawFactor"};
   TTreeReaderArray<Float_t> SubJet_tau1 = {fReader, "SubJet_tau1"};
   TTreeReaderArray<Float_t> SubJet_tau2 = {fReader, "SubJet_tau2"};
   TTreeReaderArray<Float_t> SubJet_tau3 = {fReader, "SubJet_tau3"};
   TTreeReaderArray<Float_t> SubJet_tau4 = {fReader, "SubJet_tau4"};
   TTreeReaderValue<UInt_t> nTau = {fReader, "nTau"};
   TTreeReaderArray<Float_t> Tau_chargedIso = {fReader, "Tau_chargedIso"};
   TTreeReaderArray<Float_t> Tau_dxy = {fReader, "Tau_dxy"};
   TTreeReaderArray<Float_t> Tau_dz = {fReader, "Tau_dz"};
   TTreeReaderArray<Float_t> Tau_eta = {fReader, "Tau_eta"};
   TTreeReaderArray<Float_t> Tau_leadTkDeltaEta = {fReader, "Tau_leadTkDeltaEta"};
   TTreeReaderArray<Float_t> Tau_leadTkDeltaPhi = {fReader, "Tau_leadTkDeltaPhi"};
   TTreeReaderArray<Float_t> Tau_leadTkPtOverTauPt = {fReader, "Tau_leadTkPtOverTauPt"};
   TTreeReaderArray<Float_t> Tau_mass = {fReader, "Tau_mass"};
   TTreeReaderArray<Float_t> Tau_neutralIso = {fReader, "Tau_neutralIso"};
   TTreeReaderArray<Float_t> Tau_phi = {fReader, "Tau_phi"};
   TTreeReaderArray<Float_t> Tau_photonsOutsideSignalCone = {fReader, "Tau_photonsOutsideSignalCone"};
   TTreeReaderArray<Float_t> Tau_pt = {fReader, "Tau_pt"};
   TTreeReaderArray<Float_t> Tau_puCorr = {fReader, "Tau_puCorr"};
   TTreeReaderArray<Float_t> Tau_rawAntiEle = {fReader, "Tau_rawAntiEle"};
   TTreeReaderArray<Float_t> Tau_rawIso = {fReader, "Tau_rawIso"};
   TTreeReaderArray<Float_t> Tau_rawIsodR03 = {fReader, "Tau_rawIsodR03"};
   TTreeReaderArray<Float_t> Tau_rawMVAnewDM2017v2 = {fReader, "Tau_rawMVAnewDM2017v2"};
   TTreeReaderArray<Float_t> Tau_rawMVAoldDM = {fReader, "Tau_rawMVAoldDM"};
   TTreeReaderArray<Float_t> Tau_rawMVAoldDM2017v1 = {fReader, "Tau_rawMVAoldDM2017v1"};
   TTreeReaderArray<Float_t> Tau_rawMVAoldDM2017v2 = {fReader, "Tau_rawMVAoldDM2017v2"};
   TTreeReaderArray<Float_t> Tau_rawMVAoldDMdR032017v2 = {fReader, "Tau_rawMVAoldDMdR032017v2"};
   TTreeReaderArray<Int_t> Tau_charge = {fReader, "Tau_charge"};
   TTreeReaderArray<Int_t> Tau_decayMode = {fReader, "Tau_decayMode"};
   TTreeReaderArray<Int_t> Tau_jetIdx = {fReader, "Tau_jetIdx"};
   TTreeReaderArray<Int_t> Tau_rawAntiEleCat = {fReader, "Tau_rawAntiEleCat"};
   TTreeReaderArray<UChar_t> Tau_idAntiEle = {fReader, "Tau_idAntiEle"};
   TTreeReaderArray<UChar_t> Tau_idAntiMu = {fReader, "Tau_idAntiMu"};
   TTreeReaderArray<Bool_t> Tau_idDecayMode = {fReader, "Tau_idDecayMode"};
   TTreeReaderArray<Bool_t> Tau_idDecayModeNewDMs = {fReader, "Tau_idDecayModeNewDMs"};
   TTreeReaderArray<UChar_t> Tau_idMVAnewDM2017v2 = {fReader, "Tau_idMVAnewDM2017v2"};
   TTreeReaderArray<UChar_t> Tau_idMVAoldDM = {fReader, "Tau_idMVAoldDM"};
   TTreeReaderArray<UChar_t> Tau_idMVAoldDM2017v1 = {fReader, "Tau_idMVAoldDM2017v1"};
   TTreeReaderArray<UChar_t> Tau_idMVAoldDM2017v2 = {fReader, "Tau_idMVAoldDM2017v2"};
   TTreeReaderArray<UChar_t> Tau_idMVAoldDMdR032017v2 = {fReader, "Tau_idMVAoldDMdR032017v2"};
   TTreeReaderValue<Float_t> TkMET_phi = {fReader, "TkMET_phi"};
   TTreeReaderValue<Float_t> TkMET_pt = {fReader, "TkMET_pt"};
   TTreeReaderValue<Float_t> TkMET_sumEt = {fReader, "TkMET_sumEt"};
   TTreeReaderValue<UInt_t> nTrigObj = {fReader, "nTrigObj"};
   TTreeReaderArray<Float_t> TrigObj_pt = {fReader, "TrigObj_pt"};
   TTreeReaderArray<Float_t> TrigObj_eta = {fReader, "TrigObj_eta"};
   TTreeReaderArray<Float_t> TrigObj_phi = {fReader, "TrigObj_phi"};
   TTreeReaderArray<Float_t> TrigObj_l1pt = {fReader, "TrigObj_l1pt"};
   TTreeReaderArray<Float_t> TrigObj_l1pt_2 = {fReader, "TrigObj_l1pt_2"};
   TTreeReaderArray<Float_t> TrigObj_l2pt = {fReader, "TrigObj_l2pt"};
   TTreeReaderArray<Int_t> TrigObj_id = {fReader, "TrigObj_id"};
   TTreeReaderArray<Int_t> TrigObj_l1iso = {fReader, "TrigObj_l1iso"};
   TTreeReaderArray<Int_t> TrigObj_l1charge = {fReader, "TrigObj_l1charge"};
   TTreeReaderArray<Int_t> TrigObj_filterBits = {fReader, "TrigObj_filterBits"};
   TTreeReaderValue<UInt_t> nOtherPV = {fReader, "nOtherPV"};
   TTreeReaderArray<Float_t> OtherPV_z = {fReader, "OtherPV_z"};
   TTreeReaderValue<Float_t> PV_ndof = {fReader, "PV_ndof"};
   TTreeReaderValue<Float_t> PV_x = {fReader, "PV_x"};
   TTreeReaderValue<Float_t> PV_y = {fReader, "PV_y"};
   TTreeReaderValue<Float_t> PV_z = {fReader, "PV_z"};
   TTreeReaderValue<Float_t> PV_chi2 = {fReader, "PV_chi2"};
   TTreeReaderValue<Float_t> PV_score = {fReader, "PV_score"};
   TTreeReaderValue<Int_t> PV_npvs = {fReader, "PV_npvs"};
   TTreeReaderValue<Int_t> PV_npvsGood = {fReader, "PV_npvsGood"};
   TTreeReaderValue<UInt_t> nSV = {fReader, "nSV"};
   TTreeReaderArray<Float_t> SV_dlen = {fReader, "SV_dlen"};
   TTreeReaderArray<Float_t> SV_dlenSig = {fReader, "SV_dlenSig"};
   TTreeReaderArray<Float_t> SV_pAngle = {fReader, "SV_pAngle"};
   TTreeReaderArray<UChar_t> Electron_cleanmask = {fReader, "Electron_cleanmask"};
   TTreeReaderArray<UChar_t> Jet_cleanmask = {fReader, "Jet_cleanmask"};
   TTreeReaderArray<UChar_t> Muon_cleanmask = {fReader, "Muon_cleanmask"};
   TTreeReaderArray<UChar_t> Photon_cleanmask = {fReader, "Photon_cleanmask"};
   TTreeReaderArray<UChar_t> Tau_cleanmask = {fReader, "Tau_cleanmask"};
   TTreeReaderArray<Float_t> SV_chi2 = {fReader, "SV_chi2"};
   TTreeReaderArray<Float_t> SV_eta = {fReader, "SV_eta"};
   TTreeReaderArray<Float_t> SV_mass = {fReader, "SV_mass"};
   TTreeReaderArray<Float_t> SV_ndof = {fReader, "SV_ndof"};
   TTreeReaderArray<Float_t> SV_phi = {fReader, "SV_phi"};
   TTreeReaderArray<Float_t> SV_pt = {fReader, "SV_pt"};
   TTreeReaderArray<Float_t> SV_x = {fReader, "SV_x"};
   TTreeReaderArray<Float_t> SV_y = {fReader, "SV_y"};
   TTreeReaderArray<Float_t> SV_z = {fReader, "SV_z"};
   TTreeReaderValue<Bool_t> Flag_HBHENoiseFilter = {fReader, "Flag_HBHENoiseFilter"};
   TTreeReaderValue<Bool_t> Flag_HBHENoiseIsoFilter = {fReader, "Flag_HBHENoiseIsoFilter"};
   TTreeReaderValue<Bool_t> Flag_CSCTightHaloFilter = {fReader, "Flag_CSCTightHaloFilter"};
   TTreeReaderValue<Bool_t> Flag_CSCTightHaloTrkMuUnvetoFilter = {fReader, "Flag_CSCTightHaloTrkMuUnvetoFilter"};
   TTreeReaderValue<Bool_t> Flag_CSCTightHalo2015Filter = {fReader, "Flag_CSCTightHalo2015Filter"};
   TTreeReaderValue<Bool_t> Flag_globalTightHalo2016Filter = {fReader, "Flag_globalTightHalo2016Filter"};
   TTreeReaderValue<Bool_t> Flag_globalSuperTightHalo2016Filter = {fReader, "Flag_globalSuperTightHalo2016Filter"};
   TTreeReaderValue<Bool_t> Flag_HcalStripHaloFilter = {fReader, "Flag_HcalStripHaloFilter"};
   TTreeReaderValue<Bool_t> Flag_hcalLaserEventFilter = {fReader, "Flag_hcalLaserEventFilter"};
   TTreeReaderValue<Bool_t> Flag_EcalDeadCellTriggerPrimitiveFilter = {fReader, "Flag_EcalDeadCellTriggerPrimitiveFilter"};
   TTreeReaderValue<Bool_t> Flag_EcalDeadCellBoundaryEnergyFilter = {fReader, "Flag_EcalDeadCellBoundaryEnergyFilter"};
   TTreeReaderValue<Bool_t> Flag_ecalBadCalibFilter = {fReader, "Flag_ecalBadCalibFilter"};
   TTreeReaderValue<Bool_t> Flag_goodVertices = {fReader, "Flag_goodVertices"};
   TTreeReaderValue<Bool_t> Flag_eeBadScFilter = {fReader, "Flag_eeBadScFilter"};
   TTreeReaderValue<Bool_t> Flag_ecalLaserCorrFilter = {fReader, "Flag_ecalLaserCorrFilter"};
   TTreeReaderValue<Bool_t> Flag_trkPOGFilters = {fReader, "Flag_trkPOGFilters"};
   TTreeReaderValue<Bool_t> Flag_chargedHadronTrackResolutionFilter = {fReader, "Flag_chargedHadronTrackResolutionFilter"};
   TTreeReaderValue<Bool_t> Flag_muonBadTrackFilter = {fReader, "Flag_muonBadTrackFilter"};
   TTreeReaderValue<Bool_t> Flag_BadChargedCandidateFilter = {fReader, "Flag_BadChargedCandidateFilter"};
   TTreeReaderValue<Bool_t> Flag_BadPFMuonFilter = {fReader, "Flag_BadPFMuonFilter"};
   TTreeReaderValue<Bool_t> Flag_BadChargedCandidateSummer16Filter = {fReader, "Flag_BadChargedCandidateSummer16Filter"};
   TTreeReaderValue<Bool_t> Flag_BadPFMuonSummer16Filter = {fReader, "Flag_BadPFMuonSummer16Filter"};
   TTreeReaderValue<Bool_t> Flag_trkPOG_manystripclus53X = {fReader, "Flag_trkPOG_manystripclus53X"};
   TTreeReaderValue<Bool_t> Flag_trkPOG_toomanystripclus53X = {fReader, "Flag_trkPOG_toomanystripclus53X"};
   TTreeReaderValue<Bool_t> Flag_trkPOG_logErrorTooManyClusters = {fReader, "Flag_trkPOG_logErrorTooManyClusters"};
   TTreeReaderValue<Bool_t> Flag_METFilters = {fReader, "Flag_METFilters"};


   Events(TTree *tree=0);
   virtual ~Events();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef Events_cxx
Events::Events(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   //if (tree == 0) {
   //   TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("ZZTo2L2Nu_13TeV_powheg_pythia8_RunIISummer16_NanoAODv4.root");
   //   if (!f || !f->IsOpen()) {
   //      f = new TFile("ZZTo2L2Nu_13TeV_powheg_pythia8_RunIISummer16_NanoAODv4.root");
   //   }
   //   f->GetObject("Events",tree);

   //}
   fChain = tree;
   fReader.SetTree(tree);
   //Init(tree);
}

Events::~Events()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Events::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Events::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Events::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   fChain = tree;
   fReader.SetTree(tree);
}

Bool_t Events::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Events::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Events::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Events_cxx
