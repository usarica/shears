#!/usr/bin/env python

"""Executes pruner inside a CRAB job."""

from __future__ import print_function
from datetime import datetime
import json
from operator import itemgetter
import os
import subprocess
import sys
import traceback

import ROOT

from PSet import process


def print_log(*args, **kwargs):
    """Print to log.

    Positional arguments are forwarded to print function.  If time=True
    is specified, prepent the message with a timestamp.  This is the
    only supported keyword argument.
    """
    
    if kwargs.get('time', False):
        print(datetime.now(), file=sys.stderr, end=' ')
    print(*args, file=sys.stderr)


# Predefined error codes to be reported back to CRAB.  Start from
# 100000 since CMSSW uses codes up to 99999 [1].
# [1] https://twiki.cern.ch/twiki/bin/view/CMSPublic/JobExitCodes
error_codes = {
    '':                      100000,  # Default error code
    'unhandled exception':   100001,
    'edmFileUtil error':     100002,
    'executable not found':  100003,
    'pruner error':          100004,
    'lumi mask error':       100005,
    'wrong arguments':       100006,
    'merge error':           100007
}


job_report_body = """\
<ReadBranches>
</ReadBranches>
<PerformanceReport>
  <PerformanceSummary Metric="StorageStatistics">
    <Metric Name="Parameter-untracked-bool-enabled" Value="true"/>
    <Metric Name="Parameter-untracked-bool-stats" Value="true"/>
    <Metric Name="Parameter-untracked-string-cacheHint" Value="application-only"/>
    <Metric Name="Parameter-untracked-string-readHint" Value="auto-detect"/>
    <Metric Name="ROOT-tfile-read-totalMegabytes" Value="0"/>
    <Metric Name="ROOT-tfile-write-totalMegabytes" Value="0"/>
  </PerformanceSummary>
</PerformanceReport>
<GeneratorInfo>
</GeneratorInfo>
"""

job_report_error = """\
<FrameworkError ExitStatus="{exit_code}" Type="{error_type}" >
<![CDATA[
{error_message}

]]>
</FrameworkError>
"""

def write_job_report_ok():
    """Write a trivial job report."""
    with open('FrameworkJobReport.xml', 'w') as f:
        f.write('<FrameworkJobReport>\n')
        f.write(job_report_body)
        f.write('</FrameworkJobReport>\n')


def exit_with_error(error_type, error_message=''):
    """Report an error and exit.

    Before exiting, write a job report including the error.
    """

    print_log('Encountered error "{}". Details:'.format(error_type))
    print_log(error_message)

    if error_type not in error_codes:
        # Using an unknown error type.  This must be a typo.  Use the
        # default error code.
        error_code = error_codes['']
    else:
        error_code = error_codes[error_type]

    with open('FrameworkJobReport.xml', 'w') as f:
        f.write('<FrameworkJobReport>\n')
        f.write(job_report_body)
        f.write(job_report_error.format(
            exit_code=error_code, error_type=error_type,
            error_message=error_message
        ))
        f.write('</FrameworkJobReport>\n')

    sys.exit(0)  # The steering script itself succeeds


def lfn_to_pfn(lfn):
    """Convert LFN to PFN."""
    try:
        res = subprocess.check_output(['edmFileUtil', '-d', lfn])
    except subprocess.CalledProcessError as e:
        exit_with_error(
            'edmFileUtil error',
            'Command\n  {}\nfinished with exit code {} and following '
            'output:\n{}'.format(e.cmd, e.returncode, e.output)
        )

    return res.strip()


def parse_lumi_mask(lumis):
    """Build standard lumi mask from VLuminosityBlockRange.
    
    The source representation is a list of strings of the form
    "{run_start}:{ls_start}-{run_stop}:{ls_stop}", where the part
    starting with dash can be omitted if there is only a single lumi
    section.  It is converted into a dict that maps run numbers into
    lists of pairs of lumi sections.
    """

    mask = {}

    for lumi in lumis:
        if '-' in lumi:
            start, stop = lumi.split('-')
            run_start, ls_start = [int(t) for t in start.split(':')]
            run_stop, ls_stop = [int(t) for t in stop.split(':')]
        else:
            run_start, ls_start = [int(t) for t in lumi.split(':')]
            run_stop, ls_stop = run_start, ls_start

        if run_start != run_stop:
            exit_with_error(
                'lumi mask error',
                'Failed to parse following lumi mask:\n'
                + ', '.join(lumis)
            )
        run = run_start

        if run not in mask:
            mask[run] = []

        mask[run].append([ls_start, ls_stop])

    for lumi_ranges in mask.values():
        lumi_ranges.sort(key=itemgetter(0))

    return mask


def main():
    print_log('Job script started.', time=True)

    print_log('Input arguments:', sys.argv)
    job_id = int(sys.argv[1])  # Index of current job.  Starts from 1.

    # Subsequent arguments will be forwarded to pruner, except for the
    # name for the output file, which will be treated specially
    pruner_args = []
    output_file_name = None

    for arg in sys.argv[2:]:
        if arg.startswith('--output='):
            output_file_name = arg.split('=', 1)[1]
        else:
            pruner_args.append(arg)

    if not output_file_name:
        exit_with_error('wrong arguments', 'Name for output file is missing.')


    inputs = process.source.fileNames
    print_log('Input files:')
    for path in inputs:
        print_log(' ', path)

    # The input file names are LFN.  Convert them to PFN if possible and
    # use AAA as a fallback.  Test the first file to guess if files are
    # available locally.
    use_aaa = False
    pfn = lfn_to_pfn(inputs[0])
    test_file = ROOT.TFile.Open(pfn)
    if not test_file or test_file.IsZombie():
        print_log('Could not open the first file. Switching to AAA.')
        use_aaa = True

    decoded_inputs = []
    for lfn in inputs:
        if use_aaa:
            decoded_inputs.append('root://cms-xrd-global.cern.ch/' + lfn)
        else:
            decoded_inputs.append(lfn_to_pfn(lfn))

    print_log('Decoded paths to input files:')
    for path in decoded_inputs:
        print_log(' ', path)


    if not process.source.lumisToProcess:
        lumi_mask_path = None
    else:
        lumi_mask = parse_lumi_mask(process.source.lumisToProcess)
        lumi_mask_path = 'lumis.json'
        with open(lumi_mask_path, 'w') as f:
            json.dump(lumi_mask, f)

        print_log('Lumi mask was written to file "{}".'.format(lumi_mask_path))


    if not os.path.exists('pruner'):
        exit_with_error('executable not found',
                        'Exectutable pruner is not found.')
    if not os.path.exists('haddnano.py'):
        exit_with_error('executable not found',
                        'Script haddnano.py not found.')

    # Process each input file with a separate invocation of pruner
    # because different files can have different branch structure, and
    # this is not supported by pruner
    print_log('Start processing input files.', time=True)
    temp_output_files = []

    for ifile, input_file in enumerate(decoded_inputs):
        temp_output_file = 'output_{}.root'.format(ifile)
        temp_output_files.append(temp_output_file)
        command = [
            './pruner', input_file, '-v', '--output=' + temp_output_file
        ] + pruner_args
        
        if lumi_mask_path:
            command.append('--lumi-mask=' + lumi_mask_path)

        print_log('Going to execute command ', ' '.join(command), time=True)

        try:
            subprocess.check_call(command)
        except subprocess.CalledProcessError as e:
            exit_with_error(
                'pruner error',
                'pruner terminated with error code {}.'.format(e.returncode))

        print_log('pruner terminated successfully.', time=True)
    
    if len(decoded_inputs) == 1:
        print_log(
            'Renaming the only output file "{}" to "{}".'.format(
                temp_output_files[0], output_file_name))
        os.rename(temp_output_files[0], output_file_name)
    else:
        print_log(
            'Merging individual output files to "{}".'.format(
                output_file_name),
            time=True)
        try:
            subprocess.check_call(
                ['./haddnano.py', output_file_name] + temp_output_files)
        except subprocess.CalledProcessError as e:
            exit_with_error(
                'merge error',
                'haddnano.py terminated with error code {}.'.format(
                    e.returncode)
            )
        else:
            print_log('Merging succeeded.', time=True)
    
    print_log('Job script is about to finish.', time=True)


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        traceback.print_exc(file=sys.stderr)
        exit_with_error('unhandled exception', e.message)
    else:
        write_job_report_ok()

