#!/usr/bin/env python

"""Creates pileup profiles for MC datasets."""

from __future__ import division, print_function
import argparse
import os
import subprocess
import sys
from uuid import uuid4

import ROOT
import yaml

from config import Config


def find_datasets_files(dataset_groups, ddf_dir):
    """Find MC datasets name and the correspondent skimmed root files.

    Arguments:
        dataset_group:  config.DatasetGroup object that defines a group
            of datasets.
        ddf_dir:  config.FilePath object that represents main directory to DDFS

    Return value:
      Dictionary consisting name of MC datasets as keys and list of directories
        to skimmed root files as values.
    """

    ddf_path = ddf_dir + '/{}.yaml'
    datasets_files = {}
    for dataset in dataset_groups:
        if dataset.is_sim:
            with open(ddf_path.format(dataset.name)) as f:
                ddf = yaml.safe_load(f)
                datasets_files[dataset.name] = ddf['files']

    return datasets_files



if __name__ == '__main__':

    arg_parser = argparse.ArgumentParser(description=__doc__)
    arg_parser.add_argument('config',
                            help='Configuration file with a list of datasets.')
    args = arg_parser.parse_args()
    config = Config(args.config)
    ddf_dir = config.ddf_dir().local
    pileup_dir = config.pileup_dir().srm
    pileup_profiles_path = pileup_dir + '/pileup_profiles_sim.root'
    if not os.path.exists(ddf_dir):
        raise RuntimeError(
            'Could not find DDF directory.\n\
            Please first run build_ddf <config_year.yaml>, then try again.')

    datasets_files = find_datasets_files(config.dataset_groups, ddf_dir)
    if not datasets_files:
        raise RuntimeError(
            'DDF directory seems empty.\n\
            Please first run build_ddf <config_year.yaml>, then try again.')

    subprocess.check_call(['gfal-mkdir', '-p', pileup_dir])
    tmp_path = uuid4().hex + '.root'
    pileup_profiles = ROOT.TFile.Open(tmp_path, 'RECREATE')
    for dataset, files_path in datasets_files.items():
        pileup_hist = None
        for file_path in files_path:
            root_file = ROOT.TFile(file_path)
            if pileup_hist is None:
                pileup_hist = root_file.Get('PileupProfile')
                pileup_hist.SetDirectory(None)
            else:
                pileup_hist.Add(root_file.Get('PileupProfile'))

            root_file.Close()

        pileup_profiles.WriteObject(pileup_hist, dataset)

    pileup_profiles.Close()
    subprocess.check_call(['gfal-copy', '-f', tmp_path, pileup_profiles_path])
    os.remove(tmp_path)

