# NanoAOD skimming

Basic event selection on top of NanoAOD is performed using the [`pruner`](https://gitlab.cern.ch/HZZ-IIHE/shears/tree/master/Bonzais/Pruner) utility. Tools provided here allow to run it over multiple datasets in Grid.


## Configuration file

The list of datasets to process is provided in a configuration file like [`config_2016.yaml`](config_2016.yaml), the access to which is provided with interface Python module `config.py`. Datasets with the same physics content (such as extensions) are combined into groups, each of which is treated as a single dataset in the [subsequent analysis](https://gitlab.cern.ch/HZZ-IIHE/hzz2l2nu). Every group is assigned a list of one or more selections, which are represented by pairs of strings defining selection plugin and subselection for `pruner`. Each group also has a name, which must uniquely identify the group within the campaign. It is used to construct names for CRAB tasks, output files, and inherited as the dataset name in the subsequent analysis.

Here is an example entry for a single dataset group:

```yaml
- name: ZZTo2L2Nu
  datasets:
  - /ZZTo2L2Nu_13TeV_powheg_pythia8/RunIISummer16NanoAODv5-PUMoriond17_Nano1June2019_102X_mcRun2_asymptotic_v7-v1/NANOAODSIM
  - /ZZTo2L2Nu_13TeV_powheg_pythia8_ext1/RunIISummer16NanoAODv5-PUMoriond17_Nano1June2019_102X_mcRun2_asymptotic_v7-v1/NANOAODSIM
  selections: [[ZZ2l2vPruner, MC]]
```

The configuration file also specifies the location to store the output from CRAB jobs.


## Skimming

The `crab_submit` tool can be used for massing production in the Grid as follows:

* Obtain the credentials with

  ```sh
  voms-proxy-init --voms cms
  ```

  and set up [CRAB](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideCrab) environment.
* Switch to a clean working directory:

  ```sh
  mkdir skims
  cd skims
  ```
  
* Adjust the configuration file if needed and run

  ```sh
  crab_submit config_2016.yaml
  ```
  
  You can use flag `--no-submit` to create CRAB configuration files without submitting the tasks.
* Use the standard CRAB command to check the task status or use the script `crab_check`. The latter will check all tasks in the current directory or, alternatively, tasks corresponding to the given configuration file. When a task is completed, its directory will be added to a text file with the list of completed tasks (by default, `completed_tasks.txt`) and the script willl not check its status again. Make sure that there is no such file in the directory when starting a skimming campaign. This script can also resubmit failed tasks, but make sure not to resubmit tasks with unrecoverable errors.


## Dataset definition files

After skimming jobs succeed, [dataset definition files](https://gitlab.cern.ch/HZZ-IIHE/hzz2l2nu/wikis/dataset-definitions) (DDF) should be produced for the subsequent analysis. To do it, update the VOMS proxy if needed and run

```sh
build_ddf config_2016.yaml
```

Produced files are saved in subdirectory `DDF` of the stageout directory specified in the configuration file.


## Pileup profiles

After producing DDF, concerning 2017 MC samples, pileup profile of each dataset should be prepared for offline analysis (this is due to the issue regarding pileup profiles in 2017 MC datasets, see [here](https://hypernews.cern.ch/HyperNews/CMS/get/physics-validation/3128.html)). To do it, update the VOMS proxy if needed and run

```
build_pileup_profiles config_2017.yaml
```

Produced file is saved in subdirectory `pileup` of the stageout directory specified in the configuration file, in the name of `pileup_profiles_sim.root`.
