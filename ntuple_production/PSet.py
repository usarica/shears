import FWCore.ParameterSet.Config as cms
process = cms.Process('NanoAODSkim')

process.source = cms.Source('PoolSource',
    fileNames = cms.untracked.vstring(),
    lumisToProcess = cms.untracked.VLuminosityBlockRange())
process.maxEvents = cms.untracked.PSet(input = cms.untracked.int32(-1))

